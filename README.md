Assumptions Made: 

1. Best performers refer to best intraday percentage price change
2. For statistical analysis, we would assume the traders (for day trading) would want to know the high and low of a stock during the day to the minute (or time that they check) so we have added the following fields from originally having timestamp, ticker and price:
 - open price
 - close price 
 - low 
 - high
3. The most querried field in the historical database would be date, but this is not unique as there are multiple logs for each day. (for each ticker) So we have created a unique ID colummn for a primary key. 

Questions: 

1. How many stock tickers are in the list in total?
-- select count(ticker) from profiles;

2. List all the companies and their tickers
-- select name, ticker from profiles;

3. Real time price of all the tickers?
-- select price, ticker from realTime;

4. Show all tickers displaying their company, website, industry and exchange.
-- select name, website, industry, stockExchange from profiles;

5. Which ticker has the highest real time price?
-- select name, price from profiles, realTime where profiles.ticker=realTime.ticker order by price desc limit 1;

6. Which stock has had the best intra-day performance and when was it?
-- select name, date, priceChange from historical, profiles where historical.ticker=profiles.ticker and historical.date="2019-09-17" order by priceChange DESC limit 1;

7. List all the historical open price for AAPL.
-- select openPrice from historical where ticker = 'AAPL';

8. Rank the tickers by their beta in descending order, and show their value of beta.
-- select distinct ticker, beta from historical order by beta desc;

9. which ticker has the highest open price in 2019-09-18?
-- select ticker from historical where date = '2019-09-18' order by openPrice desc limit 1;

10. Which ticker has the biggest change in open price in history? 
-- select ticker from historical where openPrice = (select max(openPrice) from historical);

11. List all the tickers in the technology sector.
-- select ticker from profiles where sector = 'Technology';





