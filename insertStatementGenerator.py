import requests
import pandas as pd
import json
from datetime import datetime
# INSERT INTO profiles VALUES("AAPL","Apple","Nasdaq Global Select","Computer Hardware","http://www.apple.com","Apple Inc is designs, manufactures and markets mobile communication and media devices and personal computers, and sells a variety of related software, services, accessories, networking solutions and third-party digital content and applications.","Timothy D. Cook","Technology","2.92",null);
# INSERT INTO historical VALUES(1,"AAPL",current_date(),84.5035, 83.6603, 83.2937, 84.7235,1.139593, 36724977, 1099398966375.00, -0.925693,  0.0);
# INSERT INTO realTime VALUES("AAPL", current_date(), 218.56, 219.875, 218.41, 220.04, 219.87);



ticker = 'AMZN'
global j
j = 1
def profilesTableInsert(ticker):
    profResp = requests.get('https://financialmodelingprep.com/api/v3/company/profile/'+ticker)
    profile = profResp.json()
    profile1 = profile['profile']
    name = profile1['companyName']
    stockExchange = profile1['exchange']
    industry = profile1['industry']
    website = profile1['website']
    description = profile1['description']
    ceo = profile1['ceo']
    sector = profile1['sector']
    lastDiv = profile1['lastDiv']
    logo = 'null'

    string = "INSERT INTO profiles VALUES(\"" + ticker + "\", \"" + name + "\",\"" + stockExchange + "\",\"" \
                + industry + "\",\"" + website + "\", \"" + description + "\", \"" + ceo + "\", \"" + sector + "\", \
                    " + lastDiv + "," + logo + ");"

    print(string)

def realTimeInsert(ticker):
    realTimeResp = requests.get('https://financialmodelingprep.com/api/v3/stock/real-time-price/' + ticker)
    realTime = realTimeResp.json()
    timestamp = datetime.now().strftime("%Y%m%d%H%M%S")
    openPrice = 'null'
    closePrice = 'null'
    low = 'null'
    high = 'null'
    price = str(realTime['price'])
    
    string = "INSERT INTO realTime VALUES(\"" + ticker + "\", " + timestamp + "," + openPrice + "," \
                + closePrice + "," + low + "," + high + ","  + price +");"

    print(string)

    
def historicalInsert(ticker):
    global j
    histResp = requests.get('https://financialmodelingprep.com/api/v3/historical-price-full/' + ticker)
    profResp = requests.get('https://financialmodelingprep.com/api/v3/company/profile/'+ticker)
    profile = profResp.json()
    profile1 = profile['profile']
    historicalWhole = histResp.json()
    historical = historicalWhole['historical']
    for a in range(1,11):
        histDict = dict(historical[-a])
        date = histDict['date'].replace('-','')
        openPrice = str(histDict['open'])
        closePrice = str(histDict['close'])
        low = str(histDict['low'])
        high = str(histDict['high'])
        beta = str(profile1['beta'])
        volume = str(histDict['volume'])
        avgMktCap = str(profile1['mktCap'])
        priceChange = str(histDict['change'])
        changeOverTime = str(histDict['changeOverTime'])
        
        string = "INSERT INTO historical VALUES("+str(j)+",\"" + ticker + "\"," + date + "," + openPrice + "," \
            + closePrice + "," + low + "," + high + "," + beta + "," + volume + "," + avgMktCap + "," + priceChange \
                + "," + changeOverTime+");"
        
        print(string)
        j+=1
    

def createInserts(ticker,id):
    profilesTableInsert(ticker)
    realTimeInsert(ticker)
    historicalInsert(ticker,id)

def main():
    j=0
    tickers = ['FDX','UPS','SNAP','AAPL','AMZN']
    for i in range(0,len(tickers)):
        profilesTableInsert(tickers[i])
        realTimeInsert(tickers[i])
        historicalInsert(tickers[i])

if __name__=="__main__":
    main()
